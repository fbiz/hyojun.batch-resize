/*
 * @version 0.3.1
 * @title   Automated resize with crop to Web
 * @author  Marcelo Miranda Carneiro <mcarneiro@fbiz.com.br>
 */
#target "photoshop"

var jsonFile = File(File($.fileName).path + '/config.json');
jsonFile.open('r');
var jsonContent = jsonFile.read();
var convertData = (new Function("return " + jsonContent))();

if (BridgeTalk.appName == "photoshop") {
	app.bringToFront;
	app.preferences.rulerUnits = Units.PIXELS;

	var inputFolder = Folder.selectDialog("Selecione a pasta com os arquivos a serem gerados:");

	if (inputFolder != null) {
		var fileList = inputFolder.getFiles(/\.(jpg|jpeg|png)$/i);

		var outputFolder = new Folder(decodeURI(inputFolder) + "/generated/");
		if (!outputFolder.exists) {
			outputFolder.create();
		}

		for (var i = 0; i < fileList.length; i++) {
			if (fileList[i] instanceof File) {
				var doc = open(fileList[i]);
				var docName = fileList[i].name.replace(/\.\w{3,4}$/, '');

				var savedState = doc.activeHistoryState;

				convert(doc, docName, convertData);
			}
		}
	}
}

function convert(doc, docName, json) {
	var data, newFile, newFolder;
	var convertData = json.photos;

	for (var i = 0; i < convertData.length; i++) {
		data = convertData[i];

		data.width = data.width != null ? data.width : json.width;
		data.height = data.height != null ? data.height : json.height;
		data.quality = data.quality != null ? data.quality : json.quality;
		data.folder = data.folder != null ? data.folder : json.folder;
		data.crop = data.crop != null ? data.crop : json.crop;

		newFolder = new Folder(decodeURI(outputFolder) + "/" + (data.folder || ''));
		newFile = new File(newFolder + "/" + data.name.replace('{name}', docName) + "." + data.format);

		if (!newFolder.exists) {
			newFolder.create();
		}

		if (doc.activeLayer.isBackgroundLayer) {
			var pixelLoc = [UnitValue(0) , UnitValue(0)]; 
			var myColorSampler = app.activeDocument.colorSamplers.add(pixelLoc); 
	
			app.backgroundColor = myColorSampler.color;
		}

		if (data.trim) {
			doc.trim(TrimType.TOPLEFT);
		}

		switch(true) {
			case (data.type === 'crop'):
				var width = doc.width;
				var height = doc.height
				var ratio = data.height / data.width;
				ratio = (ratio > 1) ? (1 / ratio) : ratio;
		
				if (width * ratio > doc.height) {
					width = height / ratio;
				} else {
					height = width * ratio;
				}
				var bounds = [
					(doc.width - width) * 0.5,
					(doc.height - height) * 0.5
				];
				bounds = bounds.concat([width + bounds[0], height + bounds[1]]);
				doc.crop(bounds, 0, data.width, data.height);
				break;
			case (data.type === 'expand'):
				if (doc.width > doc.height) {
					doc.resizeImage(UnitValue(data.width - (data.padding || 0) * 2,"px"), null, null, ResampleMethod.BICUBIC);
				} else {
					doc.resizeImage(null,UnitValue(data.height - (data.padding || 0) * 2,"px"), null, ResampleMethod.BICUBIC);
				}
				doc.resizeCanvas(data.width, data.height);
				break;
			default: //"resize only"
				if (doc.height > data.height || doc.width > data.width) {
					if (doc.width > doc.height) {
						doc.resizeImage(UnitValue(data.width,"px"),null,null,ResampleMethod.BICUBIC);
					} else {
						doc.resizeImage(null,UnitValue(data.height,"px"),null,ResampleMethod.BICUBIC);
					}
				}
				break;
		}

		exportOptions = new ExportOptionsSaveForWeb();
		switch (data.format) {
			case 'jpg':
				exportOptions.format = SaveDocumentType.JPEG;
				exportOptions.quality = data.quality || 80;
				break;
			case 'png':
				exportOptions.format = SaveDocumentType.PNG;
				exportOptions.PNG8 = false;
			break;
		}

		doc.exportDocument(newFile, ExportType.SAVEFORWEB, exportOptions);
		doc.activeHistoryState = savedState;
	}
	doc.close(SaveOptions.DONOTSAVECHANGES);
}
