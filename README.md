# Batch Resize

Photoshop script para resize de diversos arquivos. Testado com o CC 2017.

## Como usar

1. Defina as configurações no arquivo `config.json`.
2. Arraste o arquivo `_batch-resize.jsx` para dentro do photoshop.
3. Selecione a pasta com as imagens a serem alteradas.
4. 🍻

## config.json

```
{
    // qualidade do JPG
    "quality": 80, // <obrigatorio apenas para format=jpg>
    
    // lista de configurações para geração de múltiplos tamanhos
    "photos": [
        {
            // nome do arquivo onde "{name}" é o input atual,
            // pode ser concatenado
            "name": "{name}-1024x1024", // <obrigatorio>

            // largura da imagem
            "width": 1024, // <obrigatorio>

            // altura da image
            "height": 1024, // <obrigatorio>

            // espaçamento da imagem com as bordas (útil para modo "expand" + "trim")
            "padding": 80, // <opcional>

            // formato de resize:
            // - expand: como acontece no modo "contains"
            // - crop: como acontece no modo "cover"
            // - resize (default): resize para caber dentro dos bounds
            //   passados, mas não expande o tamanho total da imagem
            "type": "expand", // <obrigatorio>
            
            // antes do resize, faz um crop considerando o pixel no top left
            // como a referência do que deve ser apagado.
            "trim": true, // <opcional>

            // formato do arquivo de saída
            "format": "png" // <obrigatorio>
        }
    ]
}
```